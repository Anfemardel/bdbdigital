import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '../interfaces/location';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LocationsService {

  API_ENDPOINT = 'http://localhost:3000';
  constructor(private httpClient: HttpClient) { }
  get(){
    return this.httpClient.get(this.API_ENDPOINT);
  }
  //POST
  save(location: Location) :Observable<Location>{
    return this.httpClient.post<Location>(this.API_ENDPOINT, location);
  }
}
