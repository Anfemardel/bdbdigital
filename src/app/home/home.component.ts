import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../services/locations.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  locations: Location[];
  constructor(private locationsService: LocationsService,
    private readonly spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getLocation();
  }
  getLocation() {
    this.spinner.show();
    this.locationsService.get().subscribe((data: Location[]) => {
      this.locations = data;
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      alert('A ocurrido un error');
      this.spinner.hide();
    });
  }
}
