import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Location } from '../interfaces/location';
import { LocationsService } from '../services/locations.service';
import { FormGroup, Validators , FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public locationForm:FormGroup;
  
  constructor( private locationsService: LocationsService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
     this.createForm();
  }
  saveLocation(){
    const location: Location = this.locationForm.value;
    this.locationsService.save(location).subscribe((data) => {
      alert('Save Location');
    }, (error) => {
      console.log(error);
      alert('Ocurrio Error');
    });
  };
  private createForm(): void {
    this.locationForm = this.formBuilder.group({    
      name: ['', [Validators.required], Text],
      area_m2: ['', [Validators.required], Number]
    });
  }

}
